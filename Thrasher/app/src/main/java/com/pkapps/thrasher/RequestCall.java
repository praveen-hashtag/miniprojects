package com.pkapps.thrasher;

import android.graphics.Bitmap;
import android.util.Log;

import com.fasterxml.jackson.core.JsonFactory;
import com.google.api.client.http.HttpTransport;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Authenticator;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Credentials;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.Route;
import retrofit2.Retrofit;

public class RequestCall {

    String cred;
    public RequestCall(Bitmap bitImage) {
        this.bitImage = bitImage;
        //this.cred = cred;
    }

    OkHttpClient client = null;
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    Bitmap bitImage;
    public String buildhttpClient(){

        ByteBuffer byteBuffer = ByteBuffer.allocate(bitImage.getByteCount());
        bitImage.copyPixelsToBuffer(byteBuffer);
        byte[] bytes = byteBuffer.array();
        //String credential = "ya29.GqQBwQbnAZrM2_bK29Mo_grqr9GO40KUzhhBThQ6YrcjFku_l7cMeLo8Qu9kbATo971fiY3e5t5pCa4AmEpFq3ohgCZ5sCHQKvhej2UAiUtiiP1X4A3W42MnR5aTfspSU-EcwOngr9-d0z8CX3g8kRcphjLAjHkM-Z4M_WIxS21eRcEiX8MU5wW0v0FP-1U2g1a-bBzs0SGeVL2llVy5U8iAzqK401Y";
        //String credential = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCgNykDRCDMjOHPiPRYeaMh7HKgYOHOkKqpuUWH7geEKXaM4f2ulcjExYfhd+HsXW2JazcWRicWbsXv2YjKSQ1y3LVd1XoTDnePTF67LPzD0GjrONQmKBH55xiwxLlUdxeGX+0tjsi3qGsF/wSK1CW8Mfn2+jth9Fbq4mS+vm9q3Q6Om1Q5hOlTBQtR4oFydw7Hbss13aZphpmB0EtrabB47Aa6QKMVIZ/laNR6ryDnITHk27rjBO1wNaVeJWt+bTXJaPhVnB8ffSes7BO2aAiM1GfTrPy8T2i+tQYQF33reRWgWJ0qdIgaPl9xIf3Ta/zvChK6DkHEPlsWfQUtaAjFAgMBAAECggEASyOTiISbHVO1U6/XaOcjKvXJjI3jWQ1f/Nl3Upi/zavkfY4/e3GkByTc5VFkE4Z9qMUvwLKGAm9RRXuwO66nMAdyx98msWlHiV030HD42p0Tl72/QWXpiFppqf2ZLZPs5Ih4o7UjTX8fMmC3sKRuTiTiJoKU9wuw99fVh9+oQWVMpYK+VuEk12xHembMNYHG+lznYsiwT8ElYGrhB1N8BTdzt9iJDrOm78x/eIuIG3AepaIuZh6m8QKvD7wJ4ytdBVcu24y0MtHZ/3OIOjOPQxfw2UxTN60uwG/qE7dr2EQW2eczNFSXJPKCl2FVLpHBz74qY33lhJ5pxsqSJ3LiwQKBgQDbwq+pZOyFvBXzmf5j/POMJRZE8+KsbP548qfIDh9T/LXkS1vQtTV0snBZCfUjcEvpT0Kl4P0edkVMaS2wafOL2xxIrrb8+DEiIoV8bNaTJ9NHXEI+Bad+hULo36+NDh+nAGJYEokFXTPBBCv290IOSje4l0euxS3Gcf6rd0bU1wKBgQC6or9CI1vKIacBinZiQQZFf4gVyRwAC9hPMEWvlvTHKDnsu9F9sRuQUVOB7aLuFK6vrscoZTQTnCyVCe5SeXIQ8CFXJFVX/wA4keJURkWNdbxUbdhzQLbJdheKTU/S4gOeFWI83kj3ZfCR0QJy8Bh4sVlDmbYbkifILB65xyU/wwKBgDiu1rBSn88+3EqhBcS0kf0r87qFTYo78scvj6dCPUJkhGYGKgCDPO6EuAW/U8N4S5w6HW67wY4B44358xb1vSPtuDXnGrvVKJctYFDfodkDBLP7T/q/apxvsvpt99HiOrfQmY0t+rmveJHoiY6B6D/XVNf/7TVfFpeA/7cqZNKxAoGACfnO/UsEif5YfeWXxFKzch2J+ZghTgNUEY+D+gm4lbiZuASmAvDnh0RBkW1RSyaB29QcwxT9vpqXa3P3cCMbrLoZHh5MDYMM0PlDPrvu1cvYQC8K5cYsvwH36iWNwbjg9p8v/A7pmYeyXyjr15brx++61LkvmbOrOrDZB5cUb58CgYANKfbRwtL07RUmTgBJI6wt+7AhbiwsFdO7SWROltPWFJDu7+41USx6zVFvjDKgm3qMvLkfXe3HIQDLLHmqZcmwsqUC+zhUIBBpeI5h4U3PiIJdfSGOAqixEZWFc6yCkjNybcX/b+/duKXrnlr1rLS8tD06/X5DJAHdB5boUP7EuA==";
        //String credential = "aa1a2b15c8cb6ea54980b7a169660dbc0deb13f8";
        String credential = "AIzaSyBfxIW1dW04ZNC0CKjLpL5JdWTi1HoqLvc";
        String payload = "{payload: { image: { imageBytes:"+bytes +"    } }}";
        // String url = "https://automl.googleapis.com/v1beta1/projects/sfhacks-2019/locations/us-central1/models/ICN299703854922249396:predict";
        String url = "https://vision.googleapis.com/v1beta1/projects/";
        client = new OkHttpClient();

        RequestBody body = RequestBody.create(JSON, payload);

        Request request = new Request.Builder()
                .header("Authorization",credential)
                .url(url).post(body).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("TAG",e.getLocalizedMessage());

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.d("TAG",response.body().string());
            }
        });
        return "";
    }





}
