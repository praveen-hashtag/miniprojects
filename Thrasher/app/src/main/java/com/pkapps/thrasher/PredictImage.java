package com.pkapps.thrasher;


import android.graphics.Bitmap;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.google.cloud.automl.v1beta1.AnnotationPayload;
import com.google.cloud.automl.v1beta1.ExamplePayload;
import com.google.cloud.automl.v1beta1.Image;
import com.google.cloud.automl.v1beta1.ModelName;
import com.google.cloud.automl.v1beta1.PredictResponse;
import com.google.cloud.automl.v1beta1.PredictionServiceClient;
import com.google.protobuf.ByteString;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class PredictImage
{

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static String predict(
            String projectId,
            String computeRegion,
            String modelId,
            Bitmap bitImage
            )
            throws IOException {

        // Instantiate client for prediction service.
        PredictionServiceClient predictionClient = PredictionServiceClient.create();

        // Get the full path of the model.
        ModelName name = ModelName.of(projectId, computeRegion, modelId);

        // Read the image and assign to payload.
        ByteBuffer byteBuffer = ByteBuffer.allocate(bitImage.getByteCount());
        bitImage.copyPixelsToBuffer(byteBuffer);
        byte[] bytes = byteBuffer.array();
        ByteString content = ByteString.copyFrom(bytes);
        Image image = Image.newBuilder().setImageBytes(content).build();
        ExamplePayload examplePayload = ExamplePayload.newBuilder().setImage(image).build();

        // Additional parameters that can be provided for prediction e.g. Score Threshold
        Map<String, String> params = new HashMap<>();
//        if (scoreThreshold != null) {
//            params.put("score_threshold", scoreThreshold);
//        }
        // Perform the AutoML Prediction request
        PredictResponse response = predictionClient.predict(name, examplePayload, params);

        System.out.println("Prediction results:");
        for (AnnotationPayload annotationPayload : response.getPayloadList()) {
            System.out.println("Predicted class name :" + annotationPayload.getDisplayName());
            return annotationPayload.getDisplayName();
//            System.out.println(
//                    "Predicted class score :" + annotationPayload.getClassification().getScore());
        }
        return "empty";
    }




}
