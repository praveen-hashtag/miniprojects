package com.pkapps.thrasher;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity {

    Button camera;
    LinearLayout parent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        camera = findViewById(R.id.cameraButton);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, 1);
                }
            }
        });
        parent = findViewById(R.id.parent);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        System.out.println("Returned");
        Bitmap image = null;
        if (requestCode == 1 && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            image = (Bitmap) extras.get("data");
        }
//        try {

        if(image!=null){
            //switch ((checkColour(image)).toLowerCase()){
            switch ("paper"){
                case "paper":
                case "cardboard":
                case "metal":
                case "plastic":
                case "glass":
                        parent.setBackgroundColor(Color.parseColor("#0000FF"));
                    break;
                case "food":
                case "Disposable":
                    parent.setBackgroundColor(Color.parseColor("#00FF00"));
                    break;
                case "razors":
                case "gloves":
                case "toothpaste":
                case "sanitary":
                    parent.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    break;
                default:


            }
        }
//        }
//        catch (IOException e) {
//            e.printStackTrace();
//        }


    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public String checkColour(Bitmap image) throws IOException {
        InputStream ins = getResources().openRawResource(
                getResources().getIdentifier("credential",
                        "raw", getPackageName()));
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length;
        while ((length = ins.read(buffer)) != -1) {
            result.write(buffer, 0, length);
        }
// StandardCharsets.UTF_8.name() > JDK 7
        String file = result.toString("UTF-8");
        return new RequestCall(image).buildhttpClient();
    }
}
